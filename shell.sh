sudo mkdir /opt/shell-aprs

sudo cat > /lib/systemd/system/aprsb1.service <<- "EOF"
[Unit]
Description=APRS BEACOM1
After=syslog.target network.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/opt/shell-aprs/bcom1.sh

[Install]
WantedBy=multi-user.target

EOF
#
sudo cat > /opt/shell-aprs/bcom1.sh <<- "EOF"
#!/bin/bash
###### shellbeacon emqTE1 is shell APRS static beacon by HP3ICC
###### Works with Linux & Windows/Cygwin with netcat package installed
######
## Change the following variables to select your call, password, locaton etc.

callsign="HP3ICC-10" # Change this to your callsign-ssid
password="12345" # See http://n5dux.com/ham/aprs-passcode/
latitude="0831.27N" # Latitude information  DDMM.mm
longitude="08021.59W" # Longitude information DDDMM.mm
comment="APRS BEACON-1 / Raspbian Proyect by hp3icc" #Beacon text
simbol_primary="/" # Change this to your desired APRS symbol
simbol_secundary="r" # Change this to your desired APRS symbol
text="emq-TE1 Shell APRS Beacon"
serverHost="soam.aprs2.net" #noam.aprs2.net # See http://www.aprs2.net/APRServe2.txt
serverPort=14580 # Definable Filter Port
delay=1800 # default value 1800 to every 30 minutes




#####################################################################

address="${callsign}>APHP02,TCPIP:"
login="user $callsign pass $password vers ShellBeacon emqTE1 1.2"
packet="${address}!${latitude}${simbol_primary}${longitude}${simbol_secundary}${comment}"
packet2="${address}>${text}"
echo "$packet" # prints the packet being sent
echo "${#comment}" # prints the length of the comment part of the packet

while true
do
#### use here-document to feed packets into netcat
nc -C $serverHost $serverPort -q 10 <<-END
$login
$packet
$packet2

END
if [ "$1" = "1" ]
then
    exit
fi
sleep $delay
done

EOF
#
cp /lib/systemd/system/aprsb1.service /lib/systemd/system/aprsb2.service
cp /lib/systemd/system/aprsb1.service /lib/systemd/system/aprsb3.service
cp /lib/systemd/system/aprsb1.service /lib/systemd/system/aprsb4.service

cp /opt/shell-aprs/bcom1.sh /opt/shell-aprs/bcom2.sh
cp /opt/shell-aprs/bcom1.sh /opt/shell-aprs/bcom3.sh
cp /opt/shell-aprs/bcom1.sh /opt/shell-aprs/bcom4.sh

sudo sed -i "s/BEACOM1/BEACOM2/g"  /lib/systemd/system/aprsb2.service
sudo sed -i "s/BEACOM1/BEACOM3/g"  /lib/systemd/system/aprsb3.service
sudo sed -i "s/BEACOM1/BEACOM4/g"  /lib/systemd/system/aprsb4.service

sudo sed -i "s/bcom1/bcom2/g"  /lib/systemd/system/aprsb2.service
sudo sed -i "s/bcom1/bcom3/g"  /lib/systemd/system/aprsb3.service
sudo sed -i "s/bcom1/bcom4/g"  /lib/systemd/system/aprsb4.service

sudo sed -i "s/APRS BEACON emq-TE1/APRS BEACON-1 emq-TE1/g"   /opt/shell-aprs/bcom1.sh
sudo sed -i "s/APRS BEACON emq-TE1/APRS BEACON-2 emq-TE1/g"   /opt/shell-aprs/bcom2.sh
sudo sed -i "s/APRS BEACON emq-TE1/APRS BEACON-3 emq-TE1/g"   /opt/shell-aprs/bcom3.sh
sudo sed -i "s/APRS BEACON emq-TE1/APRS BEACON-4 emq-TE1/g"   /opt/shell-aprs/bcom4.sh

systemctl daemon-reload
sudo chmod +x /opt/shell-aprs/*

