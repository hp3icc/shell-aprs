# shell-aprs

![alt text](https://raw.githubusercontent.com/hp3icc/shell-aprs/main/shell-aprs.jpg)

#

Original reference code, simple aprs shell by wa1gov, emq-TE1 Shell APRS Beacon, includes easy configuration menu and 4 beacon options that you can configure with different messages or locations of your equipment

#

# Install

    sh -c "$(curl -fsSL https://gitlab.com/hp3icc/shell-aprs/-/raw/main/install.sh)"

#

# Setting

To configure type menu-aprs , to enter the menu

#
